package com.yuan;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
//import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.List;
import java.util.Map;

/**
 * 获取token类
 */
public class AuthService {

    /**
     * 获取权限token
     * @return 返回示例：
     * {
     * "access_token": "24.460da4889caad24cccdb1fea17221975.2592000.1491995545.282335-1234567",
     * "expires_in": 2592000
     * }
     */
    public static String getAuth() {
        // 官网获取的 API Key 更新为你注册的
        String clientId = "aeb7MSGs18PVcEPx3MQXAiKO";
        // 官网获取的 Secret Key 更新为你注册的
        String clientSecret = "qeY1i8G0CerSKsLBGm70HXwE8fXE6ZR0";
        return getAuth(clientId, clientSecret);
    }

    /**
     * 获取API访问token
     * 该token有一定的有效期，需要自行管理，当失效时需重新获取.
     * @param ak - 百度云官网获取的 API Key
     * @param sk - 百度云官网获取的 Securet Key
     * @return assess_token 示例：
     * "24.460da4889caad24cccdb1fea17221975.2592000.1491995545.282335-1234567"
     */
    public static String getAuth(String ak, String sk) {
        // 获取token地址
        String authHost = "https://aip.baidubce.com/oauth/2.0/token?";
        String getAccessTokenUrl = authHost
                // 1. grant_type为固定参数
                + "grant_type=client_credentials"
                // 2. 官网获取的 API Key
                + "&client_id=" + ak
                // 3. 官网获取的 Secret Key
                + "&client_secret=" + sk;
        try {
            URL realUrl = new URL(getAccessTokenUrl);
            // 打开和URL之间的连接
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.err.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = "";
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            /**
             * 返回结果示例
             */
            System.err.println("result:" + result);
            /*Object parse = JSONObject.parse(result);
            System.out.println("ttt:"+parse.toString());*/


            JSONObject jsonObject = JSONObject.parseObject (result);
            String access_token = jsonObject.getString("access_token");
            return access_token;
        } catch (Exception e) {
            System.err.printf("获取token失败！");
            e.printStackTrace(System.err);
        }
        return null;
    }

    public static void main(String[] args) {
      /*  String access_token =  getAuth();
        System.out.println(access_token);*/


        // 获取本地的绝对路径图片
        File file = new File("F:\\mytestFloder\\消息队列概述1.png");
        // 进行BASE64位编码
        String imageBase = BASE64.encodeImgageToBase64(file);
        imageBase = imageBase.replaceAll("\r\n", "");
        imageBase = imageBase.replaceAll("\\+", "%2B");
        // 百度云的文字识别接口,后面参数为获取到的token
        String httpUrl = "https://aip.baidubce.com/rest/2.0/ocr/v1/idcard?access_token=" + AuthService.getAuth();
        String httpArg = "detect_direction=true&id_card_side=front&image=" + imageBase;
        String jsonResult = request(httpUrl, httpArg);

        System.out.println(jsonResult);

    }


    public static String request(String httpUrl, String httpArg) {
        BufferedReader reader = null;
        String result = null;
        StringBuffer sbf = new StringBuffer();
        try {
            // 用java JDK自带的URL去请求
            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            // 设置该请求的消息头
            // 设置HTTP方法：POST
            connection.setRequestMethod("POST");
            // 设置其Header的Content-Type参数为application/x-www-form-urlencoded
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // 填入apikey到HTTP header
            connection.setRequestProperty("apikey", "uml8HFzu2hFd8iEG2LkQGMxm");
            // 将第二步获取到的token填入到HTTP header
            connection.setRequestProperty("access_token", AuthService.getAuth());
            connection.setDoOutput(true);
            connection.getOutputStream().write(httpArg.getBytes("UTF-8"));
            connection.connect();
            InputStream is = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String strRead = null;
            while ((strRead = reader.readLine()) != null) {
                sbf.append(strRead);
                sbf.append("\r\n");
            }
            reader.close();
            result = sbf.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    static class BASE64 {
        /**
         * 将本地图片进行Base64位编码
         *
         * @param imgUrl 图片的url路径，如D:\\photo\\1.png
         * @return
         */
        public  static String encodeImgageToBase64(File imageFile) {
            // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
            // 其进行Base64编码处理
            byte[] data = null;
            // 读取图片字节数组
            try {
                InputStream in = new FileInputStream(imageFile);
                data = new byte[in.available()];
                in.read(data);
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 对字节数组Base64编码
            //BASE64Encoder encoder = new BASE64Encoder();
            return base64Encoding(data);// 返回Base64编码过的字节数组字符串
        }

        /**
         * 编码图片文件，编码内容输出为{@code String}格式。
         * @param imageContents 二进制图片内容的byte数组。
         * @return {@code String}格式的编码内容。
         */
        public static String base64Encoding(byte[] imageContents) {
            if(imageContents != null)
                return Base64.getEncoder().encodeToString(imageContents);
            else return null;
        }
    }
}
