package com.yuan;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.junit.Test;

import java.awt.*;
import java.io.*;

public class Demo {
    @Test
    public void test1(){

        File imageFile = new File("F:\\mytestFloder\\消息队列概述1.png");
        if(!imageFile.exists()){
            System.out.println("图片不存在");
        }
        ITesseract instance = new Tesseract();
        instance.setDatapath("F:\\Program Files (x86)\\Tesseract-OCR\\tessdata");//设置训练库的位置
        instance.setLanguage("chi_sim");//中文识别
        String result= null;
        try {
            result = instance.doOCR(imageFile);
            System.out.println("result"+result);
        } catch (TesseractException e) {
            e.printStackTrace();
        }
    }

    /**
     * 批量转换：
     */
    @Test
    public void testBatch(){
        Long start = System.currentTimeMillis();
        System.out.println("start:"+start);
        ITesseract instance = new Tesseract();
        instance.setDatapath("F:\\Program Files (x86)\\Tesseract-OCR\\tessdata");//设置训练库的位置
        instance.setLanguage("chi_sim");//中文识别
        File file = new File("F:\\mytestFloder");//图片文件夹
        File[] listFiles = file.listFiles();
        String result= null;
        try {

            //System.out.println("result"+result);
            //遍历文件及装
            for (int i = 0; i < listFiles.length; i++) {
                //System.out.println(listFiles[i].getAbsoluteFile()+"*****"+listFiles[i].getName());
                result = instance.doOCR(listFiles[i].getAbsoluteFile());

                writeStr(result,listFiles[i].getAbsoluteFile().toString().replace("png","txt"));
            }
            Long end = System.currentTimeMillis();
            System.out.println("end:"+ end);
            System.out.println("工耗时："+(start - end));
            System.out.println("公");
        } catch (TesseractException e) {
            e.printStackTrace();
        }

    }

    /**
     * 写入txt文本文件中
     * @param result  图片中识别的文字
     * @param path   保存位置
     */
    public void writeStr(String result,String path){
        /*System.out.println(result);
        System.out.println(path);*/
        OutputStream outputStream = null;
        File file1 = new File(path);
        try {
            outputStream  = new FileOutputStream(file1);
            outputStream.write(result.getBytes());

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(outputStream !=null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Test
    public void test(){
        File file = new File("F:\\mytestFloder");
        StringBuffer stringBuffer = new StringBuffer();
        File[] listFiles = file.listFiles();
        for (int i = 0; i < listFiles.length; i++) {
            stringBuffer.append(listFiles[i].getAbsoluteFile());
            //System.out.println(listFiles[i].getAbsoluteFile()+"*****"+listFiles[i].getName());
            System.out.println(listFiles[i].getAbsoluteFile().toString().replace("png","txt"));
        }
        /*OutputStream outputStream = null;
        File file1 = new File("F:\\mytestFloder\\append.txt");
        try {
            outputStream  = new FileOutputStream(file1,true);
            //byte [] bytes = new byte[stringBuffer.toString().length()];
            outputStream.write(stringBuffer.toString().getBytes());

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(outputStream !=null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
    }
}
